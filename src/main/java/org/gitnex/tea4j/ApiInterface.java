package org.gitnex.tea4j;

import com.google.gson.JsonElement;
import org.gitnex.tea4j.annotations.Description;
import org.gitnex.tea4j.models.*;
import java.util.List;
import java.util.Map;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.*;

/**
 * Author M M Arif
 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
 */

@SuppressWarnings("unused")
@Deprecated
public interface ApiInterface {

	/**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("version")
	@Description("gitea version API without any auth")
	Call<GiteaVersion> getGiteaVersion();

	/**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("version")
	@Description("gitea version API")
	Call<GiteaVersion> getGiteaVersionWithBasic(@Header("Authorization") String authorization);

	/**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("version")
	@Description("gitea version API")
	Call<GiteaVersion> getGiteaVersionWithOTP(@Header("Authorization") String authorization, @Header("X-Gitea-OTP") int loginOTP);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("version")
	@Description("gitea version API")
	Call<GiteaVersion> getGiteaVersionWithToken(@Header("Authorization") String token);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@POST("markdown")
	Call<String> renderMarkdown(@Header("Authorization") String token, @Body MarkdownOption markdownOption);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@POST("markdown/raw")
	Call<String> renderRawMarkdown(@Header("Authorization") String token, @Body String body);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("signing-key.gpg")
	@Description("Get default signing-key.gpg")
	Call<String> getSigningKey(@Header("Authorization") String token);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("settings/api")
	@Description("Get instance's global settings for api")
	Call<APISettings> getAPISettings(@Header("Authorization") String token);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("settings/attachment")
	@Description("Get instance's global settings for attachments")
	Call<AttachmentSettings> getAttachmentSettings(@Header("Authorization") String token);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("settings/repository")
	@Description("Get instance's global settings for repositories")
	Call<RepositorySettings> getRepositorySettings(@Header("Authorization") String token);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("settings/ui")
	@Description("Get instance's global settings for ui")
	Call<UISettings> getUISettings(@Header("Authorization") String token);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("notifications")
	@Description("List users's notification threads")
	Call<List<NotificationThread>> getNotificationThreads(@Header("Authorization") String token, @Query("all") Boolean all,
		@Query("status-types") String[] statusTypes, @Query("since") String since, @Query("before") String before, @Query("page") Integer page,
		@Query("limit") Integer limit);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@PUT("notifications")
	@Description("Mark notification threads as read, pinned or unread")
	Call<Void> markNotificationThreadsAsRead(@Header("Authorization") String token, @Query("last_read_at") String last_read_at,
		@Query("all") Boolean all, @Query("status-types") String[] statusTypes, @Query("to-status") String toStatus);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("notifications/new")
	@Description("Check if unread notifications exist")
	Call<NotificationCount> checkUnreadNotifications(@Header("Authorization") String token);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("notifications/threads/{id}")
	@Description("Get notification thread by ID")
	Call<NotificationThread> getNotificationThread(@Header("Authorization") String token, @Path("id") Integer id);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@PATCH("notifications/threads/{id}")
	@Description("Mark notification thread as read by ID")
	Call<Void> markNotificationThreadAsRead(@Header("Authorization") String token, @Path("id") Integer id, @Query("to-status") String toStatus);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("orgs")
	@Description("get all organization")
	Call<List<Organization>> getAllOrgs(@Header("Authorization") String token, @Query("page") Integer page, @Query("limit") Integer limit);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@POST("orgs")
	@Description("create new organization")
	Call<UserOrganizations> createNewOrganization(@Header("Authorization") String token, @Body UserOrganizations jsonStr);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("orgs/{org}/members")
	@Description("get organization members")
	Call<List<Collaborators>> getOrgMembers(@Header("Authorization") String token, @Path("org") String ownerName);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("orgs/{owner}/labels")
	@Description("get org labels list")
	Call<List<Labels>> getOrganizationLabels(@Header("Authorization") String token, @Path("owner") String ownerName);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@POST("orgs/{owner}/labels")
	@Description("create org label")
	Call<CreateLabel> createOrganizationLabel(@Header("Authorization") String token, @Path("owner") String ownerName, @Body CreateLabel jsonStr);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@PATCH("orgs/{owner}/labels/{id}")
	@Description("update / patch org label")
	Call<CreateLabel> patchOrganizationLabel(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("id") int labelID,
		@Body CreateLabel jsonStr);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@DELETE("orgs/{owner}/labels/{id}")
	@Description("delete org label")
	Call<Labels> deleteOrganizationLabel(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("id") int labelID);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("orgs/{orgName}/repos")
	@Description("get repositories by org")
	Call<List<UserRepositories>> getReposByOrg(@Header("Authorization") String token, @Path("orgName") String orgName, @Query("page") int page,
		@Query("limit") int limit);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("orgs/{orgName}/teams")
	@Description("get teams by org")
	Call<List<Teams>> getTeamsByOrg(@Header("Authorization") String token, @Path("orgName") String orgName);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("orgs/{orgName}/members")
	@Description("get members by org")
	Call<List<UserInfo>> getMembersByOrg(@Header("Authorization") String token, @Path("orgName") String orgName);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@POST("orgs/{orgName}/teams")
	@Description("create new team")
	Call<Teams> createTeamsByOrg(@Header("Authorization") String token, @Path("orgName") String orgName, @Body Teams jsonStr);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("orgs/{orgName}")
	@Description("get an organization")
	Call<Organization> getOrganization(@Header("Authorization") String token, @Path("orgName") String orgName);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@POST("org/{org}/repos")
	@Description("create new repository under org")
	Call<OrganizationRepository> createNewUserOrgRepository(@Header("Authorization") String token, @Path("org") String orgName,
		@Body OrganizationRepository jsonStr);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("user")
	@Description("username, full name, email")
	Call<UserInfo> getUserInfo(@Header("Authorization") String token);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("user/orgs")
	@Description("get user organizations")
	Call<List<UserOrganizations>> getUserOrgs(@Header("Authorization") String token, @Query("page") int page, @Query("limit") int limit);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("user/orgs")
	@Description("get user organizations")
	Call<List<OrgOwner>> getOrgOwners(@Header("Authorization") String token, @Query("page") int page, @Query("limit") int limit);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("user/repos")
	@Description("get user repositories")
	Call<List<UserRepositories>> getUserRepositories(@Header("Authorization") String token, @Query("page") int page, @Query("limit") int limit);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@POST("user/repos")
	@Description("create new repository")
	Call<OrganizationRepository> createNewUserRepository(@Header("Authorization") String token, @Body OrganizationRepository jsonStr);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("user/followers")
	@Description("get user followers")
	Call<List<UserInfo>> getFollowers(@Header("Authorization") String token, @Query("page") int page, @Query("limit") int limit);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("user/following")
	@Description("get following")
	Call<List<UserInfo>> getFollowing(@Header("Authorization") String token, @Query("page") int page, @Query("limit") int limit);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@POST("user/emails")
	@Description("add new email")
	Call<JsonElement> addNewEmail(@Header("Authorization") String token, @Body AddEmail jsonStr);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("user/emails")
	@Description("get user emails")
	Call<List<Emails>> getUserEmails(@Header("Authorization") String token);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("user/starred")
	@Description("get user starred repositories")
	Call<List<UserRepositories>> getUserStarredRepos(@Header("Authorization") String token, @Query("page") int page, @Query("limit") int limit);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("user/starred/{owner}/{repo}")
	@Description("check star status of a repository")
	Call<JsonElement> checkRepoStarStatus(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@PUT("user/starred/{owner}/{repo}")
	@Description("star a repository")
	Call<JsonElement> starRepository(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@DELETE("user/starred/{owner}/{repo}")
	@Description("un star a repository")
	Call<JsonElement> unStarRepository(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("users/{username}")
	@Description("Get a user's profile data")
	Call<UserInfo> getUserProfile(@Header("Authorization") String token, @Path("username") String username);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("users/{username}/followers")
	@Description("List the given user's followers")
	Call<List<UserInfo>> getUserFollowers(@Header("Authorization") String token, @Path("username") String username, @Query("page") int page, @Query("limit") int limit);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("users/{username}/following")
	@Description("List the users that the given user's following")
	Call<List<UserInfo>> getUserFollowing(@Header("Authorization") String token, @Path("username") String username, @Query("page") int page, @Query("limit") int limit);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("users/{username}/repos")
	@Description("List the repos owned by the given user")
	Call<List<UserRepositories>> getUserProfileRepositories(@Header("Authorization") String token, @Path("username") String username, @Query("page") int page, @Query("limit") int limit);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("users/{username}/starred")
	@Description("List the repos that the given user has starred")
	Call<List<UserRepositories>> getUserProfileStarredRepositories(@Header("Authorization") String token, @Path("username") String username, @Query("page") int page, @Query("limit") int limit);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("users/{username}/orgs")
	@Description("List a user's organizations")
	Call<List<UserOrganizations>> getUserProfileOrganizations(@Header("Authorization") String token, @Path("username") String username, @Query("page") int page, @Query("limit") int limit);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("users/{username}/orgs/{org}/permissions")
	@Description("List differents users permissions on an organization")
	Call<OrgPermissions> getOrgPermissions(@Header("Authorization") String token, @Path("username") String username, @Path("org") String org);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("users/{username}/heatmap")
	@Description("Get a user's heatmap")
	Call<List<UserHeatmap>> getUserHeatmap(@Header("Authorization") String token, @Path("username") String username);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("user/following/{username}")
	@Description("Check whether a user is followed by the authenticated user")
	Call<JsonElement> checkFollowing(@Header("Authorization") String token, @Path("username") String username);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@PUT("user/following/{username}")
	@Description("Follow a user")
	Call<JsonElement> followUser(@Header("Authorization") String token, @Path("username") String username);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@DELETE("user/following/{username}")
	@Description("Unfollow a user")
	Call<JsonElement> unfollowUser(@Header("Authorization") String token, @Path("username") String username);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("users/search")
	@Description("search users")
	Call<UserSearch> getUserBySearch(@Header("Authorization") String token, @Query("q") String searchKeyword, @Query("limit") int limit, @Query("page") int page);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("users/{username}/tokens")
	@Description("get user token")
	Call<List<UserTokens>> getUserTokens(@Header("Authorization") String authorization, @Path("username") String loginUid);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("users/{username}/tokens")
	@Description("get user token with 2fa otp")
	Call<List<UserTokens>> getUserTokensWithOTP(@Header("Authorization") String authorization, @Header("X-Gitea-OTP") int loginOTP,
		@Path("username") String loginUid);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@POST("users/{username}/tokens")
	@Description("create new token")
	Call<UserTokens> createNewToken(@Header("Authorization") String authorization, @Path("username") String loginUid, @Body UserTokens jsonStr);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@POST("users/{username}/tokens")
	@Description("create new token with 2fa otp")
	Call<UserTokens> createNewTokenWithOTP(@Header("Authorization") String authorization, @Header("X-Gitea-OTP") int loginOTP,
		@Path("username") String loginUid, @Body UserTokens jsonStr);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@DELETE("users/{username}/tokens/{token}")
	@Description("delete token by ID")
	Call<Void> deleteToken(@Header("Authorization") String authorization, @Path("username") String loginUid, @Path("token") int tokenID);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@DELETE("users/{username}/tokens/{token}")
	@Description("delete token by ID with 2fa otp")
	Call<Void> deleteTokenWithOTP(@Header("Authorization") String authorization, @Header("X-Gitea-OTP") int loginOTP,
		@Path("username") String loginUid, @Path("token") int tokenID);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("repos/{owner}/{repo}")
	@Description("get repo information")
	Call<UserRepositories> getUserRepository(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@PATCH("repos/{owner}/{repo}")
	@Description("patch/update repository properties")
	Call<UserRepositories> updateRepositoryProperties(@Header("Authorization") String token, @Path("owner") String ownerName,
		@Path("repo") String repoName, @Body UserRepositories jsonStr);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@DELETE("repos/{owner}/{repo}")
	@Description("delete repository")
	Call<JsonElement> deleteRepository(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@POST("repos/{owner}/{repo}/transfer")
	@Description("transfer repository")
	Call<JsonElement> transferRepository(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Body RepositoryTransfer jsonStr);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("repos/{owner}/{repo}/issues")
	@Description("get issues by repo")
	Call<List<Issues>> getIssues(@Header("Authorization") String token, @Path("owner") String owner, @Path("repo") String repo,
		@Query("page") int page, @Query("limit") int limit, @Query("type") String requestType, @Query("state") String issueState, @Query("milestones") String milestones);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("repos/{owner}/{repo}/issues/{index}")
	@Description("get issue by id")
	Call<Issues> getIssueByIndex(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Path("index") int issueIndex);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("repos/{owner}/{repo}/issues/{index}/comments")
	@Description("get issue comments")
	Call<List<IssueComments>> getIssueComments(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Path("index") int issueIndex);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@POST("repos/{owner}/{repo}/issues/{index}/comments")
	@Description("reply to issue")
	Call<Issues> replyCommentToIssue(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Path("index") int issueIndex, @Body Issues jsonStr);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("repos/{owner}/{repo}/milestones")
	@Description("get milestones by repo")
	Call<List<Milestones>> getMilestones(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Query("page") int page, @Query("limit") int limit, @Query("state") String state);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("repos/{owner}/{repo}/branches")
	@Description("get branches")
	Call<List<Branches>> getBranches(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("repos/{owner}/{repo}/releases")
	@Description("get releases")
	Call<List<Releases>> getReleases(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName, @Query("page") int page, @Query("limit") int limit);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("repos/{owner}/{repo}/tags")
	@Description("get tags")
	Call<List<GitTag>> getTags(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName, @Query("page") int page, @Query("limit") int limit);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@POST("repos/{owner}/{repo}/tags")
	@Description("create a tag")
	Call<GitTag> createTag(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName, @Body CreateTagOptions jsonStr);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("repos/{owner}/{repo}/tags/{tag}")
	@Description("get a tag")
	Call<GitTag> getTag(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName, @Path("tag") String tag);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@DELETE("repos/{owner}/{repo}/tags/{tag}")
	@Description("delete a tag")
	Call<Void> deleteTag(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName, @Path("tag") String tag);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("repos/{owner}/{repo}/collaborators")
	@Description("get collaborators list")
	Call<List<Collaborators>> getCollaborators(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@POST("repos/{owner}/{repo}/milestones")
	@Description("create new milestone")
	Call<Milestones> createMilestone(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Body Milestones jsonStr);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@POST("repos/{owner}/{repo}/issues")
	@Description("create new issue")
	Call<JsonElement> createNewIssue(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Body CreateIssue jsonStr);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("repos/{owner}/{repo}/labels")
	@Description("get labels list")
	Call<List<Labels>> getLabels(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("users/{username}/repos")
	@Description("get current logged in user repositories")
	Call<List<UserRepositories>> getCurrentUserRepositories(@Header("Authorization") String token, @Path("username") String username,
		@Query("page") int page, @Query("limit") int limit);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@POST("repos/{owner}/{repo}/labels")
	@Description("create label")
	Call<CreateLabel> createLabel(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Body CreateLabel jsonStr);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@DELETE("repos/{owner}/{repo}/labels/{index}")
	@Description("delete a label")
	Call<Labels> deleteLabel(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Path("index") int labelIndex);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@PATCH("repos/{owner}/{repo}/labels/{index}")
	@Description("update / patch a label")
	Call<CreateLabel> patchLabel(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Path("index") int labelIndex, @Body CreateLabel jsonStr);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("repos/{owner}/{repo}/collaborators/{collaborator}")
	@Description("check collaborator in repo")
	Call<Collaborators> checkRepoCollaborator(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Path("collaborator") String repoCollaborator);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@DELETE("repos/{owner}/{repo}/collaborators/{username}")
	@Description("delete a collaborator from repository")
	Call<Collaborators> deleteCollaborator(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Path("username") String username);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@PUT("repos/{owner}/{repo}/collaborators/{username}")
	@Description("add a collaborator to repository")
	Call<Permission> addCollaborator(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Path("username") String username, @Body Permission jsonStr);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@PATCH("repos/{owner}/{repo}/issues/comments/{commentId}")
	@Description("edit a comment")
	Call<IssueComments> patchIssueComment(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Path("commentId") int commentId, @Body IssueComments jsonStr);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("repos/{owner}/{repo}/issues/comments/{commentId}/reactions")
	@Description("get comment reactions")
	Call<List<IssueReaction>> getIssueCommentReactions(@Header("Authorization") String token, @Path("owner") String ownerName,
		@Path("repo") String repoName, @Path("commentId") int commentId);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@POST("repos/{owner}/{repo}/issues/comments/{commentId}/reactions")
	@Description("add reaction to a comment")
	Call<IssueReaction> setIssueCommentReaction(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Path("commentId") int commentId, @Body IssueReaction jsonStr);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@HTTP(method = "DELETE", path = "repos/{owner}/{repo}/issues/comments/{commentId}/reactions", hasBody = true)
	@Description("delete a reaction of a comment")
	Call<Void> removeIssueCommentReaction(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Path("commentId") int commentId, @Body IssueReaction jsonStr);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("repos/{owner}/{repo}/issues/{index}/reactions")
	@Description("get issue reactions")
	Call<List<IssueReaction>> getIssueReactions(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Path("index") int issueIndex);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@POST("repos/{owner}/{repo}/issues/{index}/reactions")
	@Description("add reaction to an issue")
	Call<IssueReaction> setIssueReaction(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Path("index") int issueIndex, @Body IssueReaction jsonStr);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@HTTP(method = "DELETE", path = "repos/{owner}/{repo}/issues/{index}/reactions", hasBody = true)
	@Description("delete a reaction of an issue")
	Call<Void> removeIssueReaction(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Path("index") int issueIndex, @Body IssueReaction jsonStr);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("repos/{owner}/{repo}/issues/{index}/labels")
	@Description("get issue labels")
	Call<List<Labels>> getIssueLabels(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Path("index") int issueIndex);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@PUT("repos/{owner}/{repo}/issues/{index}/labels")
	@Description("replace an issue's labels")
	Call<JsonElement> updateIssueLabels(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Path("index") int issueIndex, @Body Labels jsonStr);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("repos/{owner}/{repo}/raw/{filename}")
	@Description("get file contents")
	Call<String> getFileContents(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Path("filename") String filename);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@PATCH("repos/{owner}/{repo}/issues/{issueIndex}")
	@Description("patch issue data")
	Call<JsonElement> patchIssue(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Path("issueIndex") int issueIndex, @Body CreateIssue jsonStr);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@PATCH("repos/{owner}/{repo}/issues/{issueIndex}")
	@Description("close / reopen issue")
	Call<JsonElement> closeReopenIssue(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Path("issueIndex") int issueIndex, @Body UpdateIssueState jsonStr);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@POST("repos/{owner}/{repo}/releases")
	@Description("create new release")
	Call<Releases> createNewRelease(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Body Releases jsonStr);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@PATCH("repos/{owner}/{repo}/issues/{issueIndex}")
	@Description("patch issue assignees")
	Call<JsonElement> patchIssueAssignees(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Path("issueIndex") int issueIndex, @Body UpdateIssueAssignees jsonStr);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("repos/{owner}/{repo}/stargazers")
	@Description("get all repo stars")
	Call<List<UserInfo>> getRepoStargazers(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("repos/{owner}/{repo}/subscribers")
	@Description("get all repo watchers")
	Call<List<UserInfo>> getRepoWatchers(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("repos/search")
	@Description("get all the repos which match the query string")
	Call<ExploreRepositories> queryRepos(@Header("Authorization") String token, @Query("q") String searchKeyword,
		@Query("private") Boolean repoTypeInclude, @Query("sort") String sort, @Query("order") String order, @Query("topic") boolean topic,
		@Query("includeDesc") boolean includeDesc, @Query("template") boolean template, @Query("archived") boolean archived,
		@Query("limit") int limit, @Query("page") int page);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@GET("repos/issues/search")
	@Description("get all the issues which match the query string")
	Call<List<Issues>> queryIssues(@Header("Authorization") String token, @Query("q") String searchKeyword, @Query("type") String type,
		@Query("created") Boolean created, @Query("state") String state, @Query("limit") int limit, @Query("page") int page);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@POST("repos/{owner}/{repo}/contents/{file}")
	@Description("create new file")
	Call<JsonElement> createNewFile(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Path("file") String fileName, @Body NewFile jsonStr);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@GET("repos/{owner}/{repo}/contents")
	@Description("get all the files and dirs of a repository")
	Call<List<Files>> getFiles(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Query("ref") String ref);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@GET("repos/{owner}/{repo}/contents/{filepath}")
	@Description("get single file contents")
	Call<Files> getSingleFileContents(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Path(value = "filepath", encoded = true) String filepath, @Query("ref") String ref);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@GET("repos/{owner}/{repo}/contents/{filepath}")
	@Description("get all the sub files and dirs of a repository")
	Call<List<Files>> getDirFiles(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Path(value = "filepath", encoded = true) String filepath, @Query("ref") String ref);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@HTTP(method = "DELETE", path = "repos/{owner}/{repo}/contents/{filepath}", hasBody = true)
	@Description("delete a file")
	Call<JsonElement> deleteFile(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Path(value = "filepath", encoded = true) String filepath, @Body DeleteFile jsonStr);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@PUT("repos/{owner}/{repo}/contents/{filepath}")
	@Description("edit/update a file")
	Call<JsonElement> editFile(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Path(value = "filepath", encoded = true) String filepath, @Body EditFile jsonStr);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@GET("repos/{owner}/{repo}/subscription")
	@Description("check watch status of a repository")
	Call<WatchInfo> checkRepoWatchStatus(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@PUT("repos/{owner}/{repo}/subscription")
	@Description("watch a repository")
	Call<JsonElement> watchRepository(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@DELETE("repos/{owner}/{repo}/subscription")
	@Description("un watch a repository")
	Call<JsonElement> unWatchRepository(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@GET("repos/{owner}/{repo}/issues/{index}/subscriptions/check")
	Call<WatchInfo> checkIssueWatchStatus(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Path("index") int issueIndex);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@PUT("repos/{owner}/{repo}/issues/{index}/subscriptions/{user}")
	@Description("subscribe user to issue")
	Call<Void> addIssueSubscriber(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Path("index") int issueIndex, @Path("user") String issueSubscriber);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@DELETE("repos/{owner}/{repo}/issues/{index}/subscriptions/{user}")
	@Description("unsubscribe user to issue")
	Call<Void> delIssueSubscriber(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Path("index") int issueIndex, @Path("user") String issueSubscriber);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@GET("repos/{owner}/{repo}/languages")
	@Description("Get languages and number of bytes of code written")
	Call<Map<String, Long>> getLanguages(@Header("Authorization") String token, @Path("owner") String owner, @Path("repo") String repo);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@GET("repos/{owner}/{repo}/pulls")
	@Description("get repository pull requests")
	Call<List<PullRequests>> getPullRequests(@Header("Authorization") String token, @Path("owner") String owner, @Path("repo") String repo,
		@Query("page") int page, @Query("state") String state, @Query("limit") int limit);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@Streaming
	@GET("repos/{owner}/{repo}/pulls/{index}.diff")
	@Description("get pull diff file contents")
	Call<ResponseBody> getPullDiffContent(@Header("Authorization") String token, @Path("owner") String owner, @Path("repo") String repo,
		@Path("index") String pullIndex);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@POST("repos/{owner}/{repo}/pulls/{index}/merge")
	@Description("merge a pull request")
	Call<Void> mergePullRequest(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Path("index") int index, @Body MergePullRequest jsonStr);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@POST("repos/{owner}/{repo}/pulls/{index}/update")
	@Description("Merge PR's baseBranch into headBranch")
	Call<Void> updatePullRequest(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Path("index") int index, @Query("style") String style);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@POST("repos/{owner}/{repo}/pulls")
	@Description("create a pull request")
	Call<Void> createPullRequest(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Body CreatePullRequest jsonStr);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@GET("repos/{owner}/{repo}/pulls/{index}")
	@Description("get pull request by index")
	Call<PullRequests> getPullRequestByIndex(@Header("Authorization") String token, @Path("owner") String owner, @Path("repo") String repo,
		@Path("index") int index);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@GET("repos/{owner}/{repo}/commits")
	@Description("get all commits")
	Call<List<Commits>> getRepositoryCommits(@Header("Authorization") String token, @Path("owner") String owner, @Path("repo") String repo,
		@Query("page") int page, @Query("sha") String branchName, @Query("limit") int limit);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@GET("repos/{owner}/{repo}/git/commits/{sha}")
	@Description("Get a single commit from a repository")
	Call<Commits> getCommit(@Header("Authorization") String token, @Path("owner") String owner, @Path("repo") String repo,
		@Path("sha") String sha);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@GET("repos/{owner}/{repo}/git/commits/{sha}.diff")
	@Description("Get a single commit from a repository")
	Call<ResponseBody> getCommitDiff(@Header("Authorization") String token, @Path("owner") String owner, @Path("repo") String repo,
		@Path("sha") String sha);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@PATCH("repos/{owner}/{repo}/milestones/{index}")
	@Description("close / reopen milestone")
	Call<JsonElement> closeReopenMilestone(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Path("index") int index, @Body Milestones jsonStr);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@DELETE("repos/{owner}/{repo}/issues/comments/{id}")
	@Description("delete own comment from issue")
	Call<JsonElement> deleteComment(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Path("id") int commentIndex);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@DELETE("repos/{owner}/{repo}/branches/{branch}")
	@Description("delete branch")
	Call<JsonElement> deleteBranch(@Header("Authorization") String token, @Path("owner") String ownerName, @Path("repo") String repoName,
		@Path("branch") String branchName);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@GET("repos/{owner}/{repo}/forks")
	@Description("get all repo forks")
	Call<List<UserRepositories>> getRepositoryForks(@Header("Authorization") String token, @Path("owner") String ownerName,
		@Path("repo") String repoName, @Query("page") int page, @Query("limit") int limit);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@POST("repos/{owner}/{repo}/statuses/{sha}")
	@Description("Create a commit status")
	Call<Status> createCommitStatus(@Header("Authorization") String token, @Path("owner") String owner, @Path("repo") String repo,
		@Path("sha") String sha, @Body CreateStatusOption createStatusOption);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@GET("repos/{owner}/{repo}/statuses/{sha}")
	@Description("Get a commit's statuses")
	Call<List<Status>> getCommitStatuses(@Header("Authorization") String token, @Path("owner") String owner, @Path("repo") String repo,
		@Query("sort") String sort, @Query("state") String state, @Query("page") int page, @Query("limit") int limit);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@GET("repos/{owner}/{repo}/notifications")
	@Description("List users's notification threads on a specific repo")
	Call<List<NotificationThread>> getRepoNotificationThreads(@Header("Authorization") String token, @Path("owner") String owner,
		@Path("repo") String repo, @Query("all") String all, @Query("status-types") String[] statusTypes, @Query("since") String since,
		@Query("before") String before, @Query("page") String page, @Query("limit") String limit);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@PUT("repos/{owner}/{repo}/notifications")
	@Description("Mark notification threads as read, pinned or unread on a specific repo")
	Call<Void> markRepoNotificationThreadsAsRead(@Header("Authorization") String token, @Path("owner") String owner, @Path("repo") String repo,
		@Query("all") Boolean all, @Query("status-types") String[] statusTypes, @Query("to-status") String toStatus,
		@Query("last_read_at") String last_read_at);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@GET("repos/{owner}/{repo}/assignees")
	@Description("Get all assignees")
	Call<List<Collaborators>> getAllAssignees(@Header("Authorization") String token, @Path("owner") String owner, @Path("repo") String repo);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@GET("teams/{teamIndex}/members")
	@Description("get team members by org")
	Call<List<UserInfo>> getTeamMembersByOrg(@Header("Authorization") String token, @Path("teamIndex") int teamIndex);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@GET("teams/{teamId}/members/{username}")
	@Description("check team member")
	Call<UserInfo> checkTeamMember(@Header("Authorization") String token, @Path("teamId") int teamId, @Path("username") String username);

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	@PUT("teams/{teamId}/members/{username}")
	@Description("add new team member")
	Call<JsonElement> addTeamMember(@Header("Authorization") String token, @Path("teamId") int teamId, @Path("username") String username);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@DELETE("teams/{teamId}/members/{username}")
	@Description("remove team member")
	Call<JsonElement> removeTeamMember(@Header("Authorization") String token, @Path("teamId") int teamId, @Path("username") String username);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@GET("admin/cron")
	@Description("get cron tasks")
	Call<List<CronTasks>> adminGetCronTasks(@Header("Authorization") String token, @Query("page") int page, @Query("limit") int limit);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@POST("admin/cron/{taskName}")
	@Description("run cron task")
	Call<JsonElement> adminRunCronTask(@Header("Authorization") String token, @Path("taskName") String taskName);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@POST("admin/users")
	@Description("create new user")
	Call<UserInfo> createNewUser(@Header("Authorization") String token, @Body UserInfo jsonStr);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	@GET("admin/users")
	@Description("get all users")
	Call<List<UserInfo>> adminGetUsers(@Header("Authorization") String token, @Query("page") int page, @Query("limit") int limit);

}
