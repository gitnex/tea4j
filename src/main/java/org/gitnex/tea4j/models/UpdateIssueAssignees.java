package org.gitnex.tea4j.models;

import java.io.Serializable;
import java.util.List;

/**
 * Author M M Arif
 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
 */

@Deprecated
public class UpdateIssueAssignees implements Serializable {

	private List<String> assignees;

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public UpdateIssueAssignees(List<String> assignees) {
		this.assignees = assignees;
	}

}
