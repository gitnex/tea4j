package org.gitnex.tea4j.models;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Author M M Arif
 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
 */

@Deprecated
public class PullRequests implements Serializable {

	private int id;
	private String body;
	private int comments;
	private String diff_url;
	private String html_url;
	private String merge_base;
	private String merge_commit_sha;
	private boolean mergeable;
	private boolean merged;
	private int number;
	private String patch_url;
	private String state;
	private String title;
	private String url;
	private Date closed_at;
	private Date created_at;
	private Date due_date;
	private Date merged_at;
	private Date updated_at;

	private userObject user;
	private List<labelsObject> labels;
	private List<assigneesObject> assignees;
	private mergedByObject merged_by;
	private milestoneObject milestone;
	private baseObject base;
	private headObject head;

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public PullRequests(String body) {
		this.body = body;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public static class headObject implements Serializable {

		private int repo_id;
		private String label;
		private String ref;
		private String sha;

		private repoObject repo;

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public static class repoObject implements Serializable {

			private int repo_id;
			private boolean allow_merge_commits;
			private boolean allow_rebase;
			private boolean allow_rebase_explicit;
			private boolean allow_squash_merge;
			private boolean archived;
			private boolean empty;
			private boolean fork;
			private boolean has_issues;
			private boolean has_pull_requests;
			private boolean has_wiki;
			private boolean ignore_whitespace_conflicts;
			@SerializedName("private")
			private boolean privateFlag;
			private boolean mirror;
			private String avatar_url;
			private String clone_url;
			private String default_branch;
			private String description;
			private String full_name;
			private String html_url;
			private String name;
			private String ssh_url;
			private String website;
			private int forks_count;
			private int id;
			private int open_issues_count;
			private int open_pr_counter;
			private int release_counter;
			private int size;
			private int stars_count;
			private int watchers_count;
			private Date created_at;
			private Date updated_at;

			private ownerObject owner;
			private permissionsObject permissions;

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public static class ownerObject implements Serializable {

				private int repo_id;
				private boolean is_admin;
				private String avatar_url;
				private String email;
				private String full_name;
				private String language;
				private String login;
				private Date created;

			    /**
			     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
			     */
			    @Deprecated
				public int getRepo_id() {
					return repo_id;
				}

			    /**
			     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
			     */
			    @Deprecated
				public boolean isIs_admin() {
					return is_admin;
				}

			    /**
			     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
			     */
			    @Deprecated
				public String getAvatar_url() {
					return avatar_url;
				}

			    /**
			     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
			     */
			    @Deprecated
				public String getEmail() {
					return email;
				}

			    /**
			     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
			     */
			    @Deprecated
				public String getFull_name() {
					return full_name;
				}

			    /**
			     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
			     */
			    @Deprecated
				public String getLanguage() {
					return language;
				}

			    /**
			     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
			     */
			    @Deprecated
				public String getLogin() {
					return login;
				}

			    /**
			     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
			     */
			    @Deprecated
				public Date getCreated() {
					return created;
				}
			}

			public static class permissionsObject implements Serializable {

				private boolean admin;
				private boolean pull;
				private boolean push;

				/**
				 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
				 */
				@Deprecated
				public boolean isAdmin() {
					return admin;
				}

				/**
				 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
				 */
				@Deprecated
				public boolean isPull() {
					return pull;
				}

				/**
				 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
				 */
				@Deprecated
				public boolean isPush() {
					return push;
				}
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public int getRepo_id() {
				return repo_id;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public boolean isAllow_merge_commits() {
				return allow_merge_commits;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public boolean isAllow_rebase() {
				return allow_rebase;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public boolean isAllow_rebase_explicit() {
				return allow_rebase_explicit;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public boolean isAllow_squash_merge() {
				return allow_squash_merge;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public boolean isArchived() {
				return archived;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public boolean isEmpty() {
				return empty;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public boolean isFork() {
				return fork;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public boolean isHas_issues() {
				return has_issues;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public boolean isHas_pull_requests() {
				return has_pull_requests;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public boolean isHas_wiki() {
				return has_wiki;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public boolean isIgnore_whitespace_conflicts() {
				return ignore_whitespace_conflicts;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public boolean isPrivateFlag() {
				return privateFlag;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public boolean isMirror() {
				return mirror;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public String getAvatar_url() {
				return avatar_url;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public String getClone_url() {
				return clone_url;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public String getDefault_branch() {
				return default_branch;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public String getDescription() {
				return description;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public String getFull_name() {
				return full_name;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public String getHtml_url() {
				return html_url;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public String getName() {
				return name;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public String getSsh_url() {
				return ssh_url;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public String getWebsite() {
				return website;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public int getForks_count() {
				return forks_count;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public int getId() {
				return id;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public int getOpen_issues_count() {
				return open_issues_count;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public int getOpen_pull_count() {
				return open_pr_counter;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public int getRelease_count() {
				return release_counter;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public int getSize() {
				return size;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public int getStars_count() {
				return stars_count;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public int getWatchers_count() {
				return watchers_count;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public Date getCreated_at() {
				return created_at;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public Date getUpdated_at() {
				return updated_at;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public ownerObject getOwner() {
				return owner;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public permissionsObject getPermissions() {
				return permissions;
			}
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public int getRepo_id() {

			return repo_id;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getLabel() {

			return label;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getRef() {

			return ref;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getSha() {

			return sha;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public repoObject getRepo() {

			return repo;
		}

	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public static class baseObject implements Serializable {

		private int repo_id;
		private String label;
		private String ref;
		private String sha;

		private repoObject repo;

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public static class repoObject implements Serializable {

			private int repo_id;
			private boolean allow_merge_commits;
			private boolean allow_rebase;
			private boolean allow_rebase_explicit;
			private boolean allow_squash_merge;
			private boolean archived;
			private boolean empty;
			private boolean fork;
			private boolean has_issues;
			private boolean has_pull_requests;
			private boolean has_wiki;
			private boolean ignore_whitespace_conflicts;
			@SerializedName("private")
			private boolean privateFlag;
			private boolean mirror;
			private String avatar_url;
			private String clone_url;
			private String default_branch;
			private String description;
			private String full_name;
			private String html_url;
			private String name;
			private String ssh_url;
			private String website;
			private int forks_count;
			private int id;
			private int open_issues_count;
			private int size;
			private int stars_count;
			private int watchers_count;
			private Date created_at;
			private Date updated_at;

			private ownerObject owner;
			private permissionsObject permissions;

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public static class ownerObject implements Serializable {

				private int repo_id;
				private boolean is_admin;
				private String avatar_url;
				private String email;
				private String full_name;
				private String language;
				private String login;
				private Date created;

			    /**
			     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
			     */
			    @Deprecated
				public int getRepo_id() {
					return repo_id;
				}

			    /**
			     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
			     */
			    @Deprecated
				public boolean isIs_admin() {
					return is_admin;
				}

			    /**
			     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
			     */
			    @Deprecated
				public String getAvatar_url() {
					return avatar_url;
				}

			    /**
			     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
			     */
			    @Deprecated
				public String getEmail() {
					return email;
				}

			    /**
			     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
			     */
			    @Deprecated
				public String getFull_name() {
					return full_name;
				}

			    /**
			     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
			     */
			    @Deprecated
				public String getLanguage() {
					return language;
				}

			    /**
			     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
			     */
			    @Deprecated
				public String getLogin() {
					return login;
				}

			    /**
			     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
			     */
			    @Deprecated
				public Date getCreated() {
					return created;
				}
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public static class permissionsObject implements Serializable {

				private boolean admin;
				private boolean pull;
				private boolean push;

			    /**
			     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
			     */
			    @Deprecated
				public boolean isAdmin() {
					return admin;
				}

			    /**
			     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
			     */
			    @Deprecated
				public boolean isPull() {
					return pull;
				}

			    /**
			     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
			     */
			    @Deprecated
				public boolean isPush() {
					return push;
				}
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public int getRepo_id() {
				return repo_id;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public boolean isAllow_merge_commits() {
				return allow_merge_commits;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public boolean isAllow_rebase() {
				return allow_rebase;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public boolean isAllow_rebase_explicit() {
				return allow_rebase_explicit;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public boolean isAllow_squash_merge() {
				return allow_squash_merge;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public boolean isArchived() {
				return archived;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public boolean isEmpty() {
				return empty;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public boolean isFork() {
				return fork;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public boolean isHas_issues() {
				return has_issues;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public boolean isHas_pull_requests() {
				return has_pull_requests;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public boolean isHas_wiki() {
				return has_wiki;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public boolean isIgnore_whitespace_conflicts() {
				return ignore_whitespace_conflicts;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public boolean isPrivateFlag() {
				return privateFlag;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public boolean isMirror() {
				return mirror;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public String getAvatar_url() {
				return avatar_url;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public String getClone_url() {
				return clone_url;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public String getDefault_branch() {
				return default_branch;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public String getDescription() {
				return description;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public String getFull_name() {
				return full_name;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public String getHtml_url() {
				return html_url;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public String getName() {
				return name;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public String getSsh_url() {
				return ssh_url;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public String getWebsite() {
				return website;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public int getForks_count() {
				return forks_count;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public int getId() {
				return id;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public int getOpen_issues_count() {
				return open_issues_count;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public int getSize() {
				return size;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public int getStars_count() {
				return stars_count;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public int getWatchers_count() {
				return watchers_count;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public Date getCreated_at() {
				return created_at;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public Date getUpdated_at() {
				return updated_at;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public ownerObject getOwner() {
				return owner;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public permissionsObject getPermissions() {
				return permissions;
			}
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public int getRepo_id() {

			return repo_id;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getLabel() {

			return label;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getRef() {

			return ref;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getSha() {

			return sha;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public repoObject getRepo() {

			return repo;
		}

	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public static class userObject implements Serializable {

		private int id;
		private String login;
		private String full_name;
		private String email;
		private String avatar_url;
		private String language;
		private boolean is_admin;

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public int getId() {
			return id;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getLogin() {
			return login;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getFull_name() {
			return full_name;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getEmail() {
			return email;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getAvatar_url() {
			return avatar_url;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getLanguage() {
			return language;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public boolean isIs_admin() {
			return is_admin;
		}
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public static class labelsObject implements Serializable {

		private int id;
		private String name;
		private String color;
		private String url;

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public int getId() {
			return id;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getName() {
			return name;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getColor() {
			return color;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getUrl() {
			return url;
		}
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public static class assigneesObject implements Serializable {

		private int id;
		private String login;
		private String full_name;
		private String email;
		private String avatar_url;
		private String language;
		private boolean is_admin;

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public int getId() {
			return id;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getLogin() {
			return login;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getFull_name() {
			return full_name;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getEmail() {
			return email;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getAvatar_url() {
			return avatar_url;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getLanguage() {
			return language;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public boolean isIs_admin() {
			return is_admin;
		}
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public static class mergedByObject implements Serializable {

		private int id;
		private String login;
		private String full_name;
		private String email;
		private String avatar_url;
		private String language;
		private boolean is_admin;

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public int getId() {
			return id;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getLogin() {
			return login;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getFull_name() {
			return full_name;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getEmail() {
			return email;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getAvatar_url() {
			return avatar_url;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getLanguage() {
			return language;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public boolean isIs_admin() {
			return is_admin;
		}
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public static class milestoneObject implements Serializable {

		private int id;
		private String title;
		private String description;
		private String state;
		private String open_issues;
		private String closed_issues;
		private String closed_at;
		private String due_on;

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public int getId() {
			return id;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getTitle() {
			return title;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getDescription() {
			return description;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getState() {
			return state;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getOpen_issues() {
			return open_issues;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getClosed_issues() {
			return closed_issues;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getClosed_at() {
			return closed_at;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getDue_on() {
			return due_on;
		}
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public int getId() {
		return id;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getBody() {
		return body;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public int getComments() {
		return comments;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getDiff_url() {
		return diff_url;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getHtml_url() {
		return html_url;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getMerge_base() {
		return merge_base;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getMerge_commit_sha() {
		return merge_commit_sha;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public boolean isMergeable() {
		return mergeable;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public boolean isMerged() {
		return merged;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public int getNumber() {
		return number;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getPatch_url() {
		return patch_url;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getState() {
		return state;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getTitle() {
		return title;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getUrl() {
		return url;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public Date getClosed_at() {
		return closed_at;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public Date getCreated_at() {
		return created_at;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public Date getDue_date() {
		return due_date;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public Date getMerged_at() {
		return merged_at;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public Date getUpdated_at() {
		return updated_at;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public userObject getUser() {
		return user;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public List<labelsObject> getLabels() {
		return labels;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public List<assigneesObject> getAssignees() {
		return assignees;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public mergedByObject getMerged_by() {
		return merged_by;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public milestoneObject getMilestone() {
		return milestone;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public baseObject getBase() {
		return base;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public headObject getHead() {
		return head;
	}
}
