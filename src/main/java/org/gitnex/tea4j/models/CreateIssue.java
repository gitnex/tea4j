package org.gitnex.tea4j.models;

import java.io.Serializable;
import java.util.List;

/**
 * Author M M Arif
 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
 */

@Deprecated
public class CreateIssue implements Serializable {

	private String body;
	private boolean closed;
	private String due_date;
	private int milestone;
	private String title;

	private List<String> assignees;
	private List<Integer> labels;

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public CreateIssue(String assignee, String body, boolean closed, String due_date, int milestone, String title, List<String> assignees, List<Integer> labels) {
		this.body = body;
		this.closed = closed;
		this.due_date = due_date;
		this.milestone = milestone;
		this.title = title;
		this.assignees = assignees;
		this.labels = labels;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public CreateIssue(String title, String body, String due_date, int milestone) {
		this.title = title;
		this.body = body;
		this.due_date = due_date;
		this.milestone = milestone;
	}

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	private static class Assignees implements Serializable {
	}

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	private static class Labels implements Serializable {
	}
}
