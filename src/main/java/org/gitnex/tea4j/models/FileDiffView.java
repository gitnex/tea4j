package org.gitnex.tea4j.models;

import java.io.Serializable;
import java.util.List;

/**
 * Author M M Arif
 * Author 6543
 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
 */

@Deprecated
public class FileDiffView implements Serializable {

	private String fileNewName;
	private String fileOldName;
	private String diffType;
	private String fileInfo;
	private Stats stats;
	private List<Content> contents;

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public static class Stats implements Serializable {

		private int lineAdded;
		private int lineRemoved;

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public Stats(int added, int removed) {

			this.lineAdded = added;
			this.lineRemoved = removed;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public int getAdded() {

			return lineAdded;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public int getRemoved() {

			return lineRemoved;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
	    @Override
		public String toString() {

			return "+" + this.lineAdded + ", -" + this.lineRemoved;
		}

	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public static class Content implements Serializable {

		private int lineAdded;
		private int lineRemoved;
		private int oldLineStart;
		private int newLineStart;
		private String raw;

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public Content(String content) {

			this.raw = content;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public Content(String content, int oldStart, int newStart, int removed, int added) {

			this.raw = content;
			this.lineAdded = added;
			this.lineRemoved = removed;
			this.oldLineStart = oldStart;
			this.newLineStart = newStart;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getRaw() {

			return raw;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public int getLineAdded() {

			return this.lineAdded;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public int getLineRemoved() {

			return this.lineRemoved;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public int getOldLineStart() {

			return this.oldLineStart;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public int getNewLineStart() {

			return this.newLineStart;
		}

	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public FileDiffView(String oldName, String newName, String diffType, String fileInfo, List<Content> fileContents) {

		this.fileNewName = newName.trim();
		this.fileOldName = oldName.trim();
		this.diffType = diffType;
		this.fileInfo = fileInfo;
		this.contents = fileContents;
		this.stats = new Stats(0, 0);
		if(fileContents != null) {
			for(Content content : this.contents) {
				stats.lineAdded += content.lineAdded;
				stats.lineRemoved += content.lineRemoved;
			}
		}

	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getFileName() {

		if(fileOldName.length() != 0 && !fileOldName.equals(fileNewName)) {
			return fileOldName + " -> " + fileNewName;
		}
		return fileNewName;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public boolean isFileBinary() {

		return diffType.equals("binary");
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getFileInfo() {

		if(diffType.equals("binary")) {
			return diffType + " " + fileInfo;
		}

		if(fileInfo.equals("change") && this.stats != null) {
			return this.stats.toString();
		}

		return fileInfo;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String toString() {

		StringBuilder raw = new StringBuilder();
		if(this.contents != null) {
			for(Content c : this.contents) {
				raw.append(c.getRaw());
			}
		}
		return raw.toString();
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public List<Content> getFileContents() {

		return this.contents;
	}

}
