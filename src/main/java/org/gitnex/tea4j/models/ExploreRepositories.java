package org.gitnex.tea4j.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Author M M Arif
 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
 */

@Deprecated
public class ExploreRepositories implements Serializable {

	private ArrayList<UserRepositories> data;
	private Boolean ok;

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public ArrayList<UserRepositories> getSearchedData() {
		return data;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public Boolean getOk() {
		return ok;
	}

}
