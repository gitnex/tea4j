package org.gitnex.tea4j.models;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.Date;

/**
 * Author M M Arif
 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
 */

@Deprecated
public class UserRepositories implements Serializable {

	private int id;
	private String name;
	private String full_name;
	private String description;
	@SerializedName("private")
	private boolean privateFlag;
	private String stars_count;
	private String watchers_count;
	private String open_issues_count;
	private String open_pr_counter;
	private String release_counter;
	private String html_url;
	private String default_branch;
	private Date created_at;
	private Date updated_at;
	private String clone_url;
	private long size;
	private String ssh_url;
	private String website;
	private String forks_count;
	private Boolean has_issues;
	private String avatar_url;
	private boolean archived;
	private boolean allow_merge_commits;
	private boolean allow_rebase;
	private boolean allow_rebase_explicit;
	private boolean allow_squash_merge;
	private boolean has_pull_requests;
	private boolean has_wiki;
	private boolean ignore_whitespace_conflicts;
	private boolean template;

	private permissionsObject permissions;
	private externalIssueTrackerObject external_tracker;
	private externalWikiObject external_wiki;
	private internalTimeTrackerObject internal_tracker;

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public UserRepositories(String body) {
		this.name = name;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public UserRepositories(String name, String website, String description,
		boolean repoPrivate, boolean repoAsTemplate, boolean repoEnableIssues,
		boolean repoEnableWiki, boolean repoEnablePr,
		boolean repoEnableMerge, boolean repoEnableRebase, boolean repoEnableSquash, boolean repoEnableForceMerge) {

		this.name = name;
		this.website = website;
		this.description = description;
		this.privateFlag = repoPrivate;
		this.template = repoAsTemplate;
		this.has_issues = repoEnableIssues;
		this.has_wiki = repoEnableWiki;
		this.has_pull_requests = repoEnablePr;
		this.allow_merge_commits = repoEnableMerge;
		this.allow_rebase = repoEnableRebase;
		this.allow_squash_merge = repoEnableSquash;
		this.allow_rebase_explicit = repoEnableForceMerge;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public UserRepositories(String name, String website, String description,
		boolean repoPrivate, boolean repoAsTemplate, boolean repoEnableIssues,
		boolean repoEnableWiki, boolean repoEnablePr, internalTimeTrackerObject repoEnableTimer,
		boolean repoEnableMerge, boolean repoEnableRebase, boolean repoEnableSquash, boolean repoEnableForceMerge) {

		this.name = name;
		this.website = website;
		this.description = description;
		this.privateFlag = repoPrivate;
		this.template = repoAsTemplate;
		this.has_issues = repoEnableIssues;
		this.has_wiki = repoEnableWiki;
		this.has_pull_requests = repoEnablePr;
		this.internal_tracker = repoEnableTimer;
		this.allow_merge_commits = repoEnableMerge;
		this.allow_rebase = repoEnableRebase;
		this.allow_squash_merge = repoEnableSquash;
		this.allow_rebase_explicit = repoEnableForceMerge;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public static class internalTimeTrackerObject implements Serializable {

		private boolean allow_only_contributors_to_track_time;
		private boolean enable_issue_dependencies;
		private boolean enable_time_tracker;

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public internalTimeTrackerObject(boolean enable_time_tracker) {

			this.enable_time_tracker = enable_time_tracker;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public boolean isAllow_only_contributors_to_track_time() {

			return allow_only_contributors_to_track_time;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public boolean isEnable_issue_dependencies() {

			return enable_issue_dependencies;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public boolean isEnable_time_tracker() {

			return enable_time_tracker;
		}

	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public static class externalWikiObject implements Serializable {

		private String external_wiki_url;

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public externalWikiObject(String external_wiki_url) {

			this.external_wiki_url = external_wiki_url;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getExternal_wiki_url() {

			return external_wiki_url;
		}

	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public static class externalIssueTrackerObject implements Serializable {

		private String external_tracker_format;
		private String external_tracker_style;
		private String external_tracker_url;

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public externalIssueTrackerObject(String external_tracker_url) {

			this.external_tracker_url = external_tracker_url;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getExternal_tracker_format() {

			return external_tracker_format;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getExternal_tracker_style() {

			return external_tracker_style;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getExternal_tracker_url() {

			return external_tracker_url;
		}

	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public static class permissionsObject implements Serializable {

		private boolean admin;
		private boolean push;
		private boolean pull;

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public boolean isAdmin() {

			return admin;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public boolean canPush() {

			return push;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public boolean canPull() {

			return pull;
		}

	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public int getId() {

		return id;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getName() {

		return name;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getFullName() {

		return full_name;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getDescription() {

		return description;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public permissionsObject getPermissions() {

		return permissions;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public Boolean getPrivateFlag() {

		return privateFlag;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getStars_count() {

		return stars_count;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getOpen_pull_count() {

		return open_pr_counter;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getRelease_count() {

		return release_counter;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getWatchers_count() {

		return watchers_count;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getOpen_issues_count() {

		return open_issues_count;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getHtml_url() {

		return html_url;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getDefault_branch() {

		return default_branch;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public Date getCreated_at() {

		return created_at;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public Date getUpdated_at() {

		return updated_at;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getClone_url() {

		return clone_url;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public long getSize() {

		return size;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getSsh_url() {

		return ssh_url;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getWebsite() {

		return website;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getForks_count() {

		return forks_count;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public Boolean getHas_issues() {

		return has_issues;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getAvatar_url() {

		return avatar_url;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public boolean isPrivateFlag() {

		return privateFlag;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getOpen_pr_counter() {

		return open_pr_counter;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getRelease_counter() {

		return release_counter;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public boolean isArchived() {

		return archived;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getFull_name() {

		return full_name;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public boolean isAllow_merge_commits() {

		return allow_merge_commits;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public boolean isAllow_rebase() {

		return allow_rebase;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public boolean isAllow_rebase_explicit() {

		return allow_rebase_explicit;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public boolean isAllow_squash_merge() {

		return allow_squash_merge;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public boolean isHas_pull_requests() {

		return has_pull_requests;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public boolean isHas_wiki() {

		return has_wiki;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public boolean isIgnore_whitespace_conflicts() {

		return ignore_whitespace_conflicts;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public boolean isTemplate() {

		return template;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public externalIssueTrackerObject getExternal_tracker() {

		return external_tracker;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public externalWikiObject getExternal_wiki() {

		return external_wiki;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public internalTimeTrackerObject getInternal_tracker() {

		return internal_tracker;
	}

}
