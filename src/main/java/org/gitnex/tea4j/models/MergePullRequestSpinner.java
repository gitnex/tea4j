package org.gitnex.tea4j.models;

import java.io.Serializable;

/**
 * Author M M Arif
 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
 */

@Deprecated
public class MergePullRequestSpinner implements Serializable {

	private String id;
	private String mergerMethod;

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public MergePullRequestSpinner(String id, String mergerMethod) {
		this.id = id;
		this.mergerMethod = mergerMethod;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getId() {
		return id;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public void setId(String id) {
		this.id = id;
	}

	private String getMergerMethod() {
		return mergerMethod;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public void setName(String mergerMethod) {
		this.mergerMethod = mergerMethod;
	}

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Override
	@Deprecated
	public String toString() {
		return mergerMethod;
	}

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Override
	@Deprecated
	public boolean equals(Object obj) {

		if(obj instanceof MergePullRequestSpinner){

			MergePullRequestSpinner spinner = (MergePullRequestSpinner )obj;
			return spinner.getMergerMethod().equals(mergerMethod) && spinner.getId().equals(id);

		}

		return false;
	}

}
