package org.gitnex.tea4j.models;

import java.io.Serializable;

/**
 * Author M M Arif
 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
 */

@Deprecated
public class MergePullRequest implements Serializable {

	private String Do;
	private String MergeMessageField;
	private String MergeTitleField;

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public MergePullRequest(String Do, String MergeMessageField, String MergeTitleField) {

		this.Do = Do;
		this.MergeMessageField = MergeMessageField;
		this.MergeTitleField = MergeTitleField;

	}

}
