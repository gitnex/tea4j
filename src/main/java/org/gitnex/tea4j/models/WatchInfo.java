package org.gitnex.tea4j.models;

import java.io.Serializable;

/**
 * Author M M Arif
 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
 */

@Deprecated
public class WatchInfo implements Serializable {

	private Boolean subscribed;
	private Boolean ignored; // = !subscribed
	private String reason;   // not used by gitea jet
	private String created_at;
	private String url;
	private String repository_url;

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public Boolean getSubscribed() {

		return subscribed;

	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getCreated_at() {

		return created_at;

	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getUrl() {

		return url;

	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getRepository_url() {

		return repository_url;

	}

}
