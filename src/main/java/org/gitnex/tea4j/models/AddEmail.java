package org.gitnex.tea4j.models;

import java.io.Serializable;
import java.util.List;

/**
 * Author M M Arif
 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
 */

@Deprecated
public class AddEmail implements Serializable {

	private List<String> emails;

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public AddEmail(List<String> emails) {
		this.emails = emails;
	}

}
