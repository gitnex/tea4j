package org.gitnex.tea4j.models;

import java.io.Serializable;

/**
 * Author opyale
 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
 */

@Deprecated
public class APISettings implements Serializable {

	private int default_git_trees_per_page;
	private int default_max_blob_size;
	private int default_paging_num;
	private int max_response_items;

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public int getDefault_git_trees_per_page() {

		return default_git_trees_per_page;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public int getDefault_max_blob_size() {

		return default_max_blob_size;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public int getDefault_paging_num() {

		return default_paging_num;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public int getMax_response_items() {

		return max_response_items;
	}

}
