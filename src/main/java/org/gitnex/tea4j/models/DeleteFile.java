package org.gitnex.tea4j.models;

import java.io.Serializable;

/**
 * Author M M Arif
 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
 */

@Deprecated
public class DeleteFile implements Serializable {

	private String branch;
	private String message;
	private String new_branch;
	private String sha;

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getBranch() {

		return branch;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public void setBranch(String branch) {

		this.branch = branch;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getMessage() {

		return message;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public void setMessage(String message) {

		this.message = message;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getNew_branch() {

		return new_branch;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public void setNew_branch(String new_branch) {

		this.new_branch = new_branch;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getSha() {

		return sha;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public void setSha(String sha) {

		this.sha = sha;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public DeleteFile(String branch, String message, String new_branch, String sha) {
		this.branch = branch;
		this.message = message;
		this.new_branch = new_branch;
		this.sha = sha;
	}
}
