package org.gitnex.tea4j.models;

import java.io.Serializable;
import java.util.List;

/**
 * Author M M Arif
 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
 */

@Deprecated
public class Labels implements Serializable {

	private int id;
	private String name;
	private String color;
	private String url;
	private List<Integer> labels;

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public Labels(String name, String color) {
		this.name = name;
		this.color = color;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public Labels(List<Integer> labels) {
		this.labels = labels;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public Labels(int id, String name, String color) {
		this.id = id;
		this.name = name;
		this.color = color;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public int getId() {
		return id;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getName() {
		return name;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getColor() {
		return color;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getUrl() {
		return url;
	}
}
