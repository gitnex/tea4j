package org.gitnex.tea4j.models;

import java.io.Serializable;

/**
 * Author qwerty287
 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
 */

@Deprecated
public class OrgPermissions implements Serializable {

	private Boolean can_create_repository;
	private Boolean can_read;
	private Boolean can_write;
	private Boolean is_admin;
	private Boolean is_owner;

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public Boolean canCreateRepositories() {
		return can_create_repository;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public Boolean canRead() {
		return can_read;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public Boolean canWrite() {
		return can_write;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public Boolean isAdmin() {
		return is_admin;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public Boolean isOwner() {
		return is_owner;
	}

}
