package org.gitnex.tea4j.models;

import java.io.Serializable;
import java.util.Date;

/**
 * Author M M Arif
 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
 */

@Deprecated
public class Branches implements Serializable {

	private String name;

	private commitObject commit;

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public Branches(String name) {
		this.name = name;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getName() {
		return name;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public static class commitObject implements Serializable {

		private String id;
		private String message;
		private String url;
		private Date timestamp;

		private authorObject author;

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public static class authorObject implements Serializable {

			private String name;
			private String email;
			private String username;

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public String getName() {
				return name;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public String getEmail() {
				return email;
			}

		    /**
		     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
		     */
		    @Deprecated
			public String getUsername() {
				return username;
			}
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getId() {
			return id;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getMessage() {
			return message;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public authorObject getAuthor() {
			return author;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getUrl() {
			return url;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public Date getTimestamp() {
			return timestamp;
		}
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public commitObject getCommit() {
		return commit;
	}

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Override
	@Deprecated
	public String toString() {
		return name;
	}
}
