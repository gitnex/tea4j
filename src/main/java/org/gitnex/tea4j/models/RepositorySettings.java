package org.gitnex.tea4j.models;

import java.io.Serializable;

/**
 * Author opyale
 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
 */

@Deprecated
public class RepositorySettings implements Serializable {

	private boolean http_git_disabled;
	private boolean mirrors_disabled;

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public boolean isHttp_git_disabled() {

		return http_git_disabled;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public boolean isMirrors_disabled() {

		return mirrors_disabled;
	}

}
