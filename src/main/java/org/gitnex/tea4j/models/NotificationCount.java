package org.gitnex.tea4j.models;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/**
 * Author M M Arif
 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
 */

@Deprecated
public class NotificationCount implements Serializable {

	@SerializedName("new")
	private int counter;

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public int getCounter() {

		return counter;
	}

}
