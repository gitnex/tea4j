package org.gitnex.tea4j.models;

import java.io.Serializable;

/**
 * Author qwerty287
 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
 */

@Deprecated
public class CreateTagOptions implements Serializable {

	private String message;
	private String tag_name;
	private String target;

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public CreateTagOptions(String message, String tagName, String target) {
		this.message = message;
		this.tag_name = tagName;
		this.target = target;
	}
}
