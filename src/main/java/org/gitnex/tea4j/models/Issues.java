package org.gitnex.tea4j.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Author M M Arif
 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
 */

@Deprecated
public class Issues implements Serializable {

	private int id;
	private String url;
	private String html_url;
	private int number;
	private String title;
	private String body;
	private String state;
	private int comments;
	private Date created_at;
	private Date updated_at;
	private Date due_date;
	private Date closed_at;

	private userObject user;
	private List<labelsObject> labels;
	private pullRequestObject pull_request;
	private milestoneObject milestone;
	private List<assigneesObject> assignees;
	private repositoryObject repository;

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public Issues(String body) {
		this.body = body;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public static class userObject implements Serializable {

		private int id;
		private String login;
		private String full_name;
		private String email;
		private String avatar_url;
		private String language;
		private String username;

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public int getId() {
			return id;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getLogin() {
			return login;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getFull_name() {
			return full_name;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getEmail() {
			return email;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getAvatar_url() {
			return avatar_url;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getLanguage() {
			return language;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getUsername() {
			return username;
		}

	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public static class labelsObject implements Serializable {

		private int id;
		private String name;
		private String color;
		private String url;

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public int getId() {
			return id;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getName() {
			return name;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getColor() {
			return color;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getUrl() {
			return url;
		}
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public static class pullRequestObject implements Serializable {

		private boolean merged;
		private String merged_at;

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public boolean isMerged() {
			return merged;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getMerged_at() {
			return merged_at;
		}
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public static class milestoneObject implements Serializable {

		private int id;
		private String title;
		private String description;
		private String state;
		private String open_issues;
		private String closed_issues;
		private String closed_at;
		private String due_on;

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public int getId() {
			return id;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getTitle() {
			return title;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getDescription() {
			return description;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getState() {
			return state;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getOpen_issues() {
			return open_issues;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getClosed_issues() {
			return closed_issues;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getClosed_at() {
			return closed_at;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getDue_on() {
			return due_on;
		}
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public static class assigneesObject implements Serializable {

		private int id;
		private String login;
		private String full_name;
		private String email;
		private String avatar_url;
		private String language;
		private String username;

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public int getId() {
			return id;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getLogin() {
			return login;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getFull_name() {
			return full_name;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getEmail() {
			return email;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getAvatar_url() {
			return avatar_url;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getLanguage() {
			return language;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getUsername() {
			return username;
		}
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public static class repositoryObject implements Serializable {

		private int id;
		private String full_name;
		private String name;
		private String owner;

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public int getId() {

			return id;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getFull_name() {

			return full_name;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getName() {

			return name;
		}

	    /**
	     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	     */
	    @Deprecated
		public String getOwner() {

			return owner;
		}

	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public int getId() {
		return id;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getUrl() {
		return url;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public int getNumber() {
		return number;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getTitle() {
		return title;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getBody() {
		return body;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getState() {
		return state;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public int getComments() {
		return comments;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public Date getCreated_at() {
		return created_at;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public Date getUpdated_at() {
		return updated_at;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public Date getDue_date() {
		return due_date;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public Date getClosed_at() {
		return closed_at;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public userObject getUser() {
		return user;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public List<labelsObject> getLabels() {
		return labels;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public pullRequestObject getPull_request() {
		return pull_request;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public milestoneObject getMilestone() {
		return milestone;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public List<assigneesObject> getAssignees() {
		return assignees;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getHtml_url() {

		return html_url;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public void setHtml_url(String html_url) {

		this.html_url = html_url;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public repositoryObject getRepository() {

		return repository;
	}

}
