package org.gitnex.tea4j.models;

import java.io.Serializable;
import java.util.Date;

/**
 * Author M M Arif
 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
 */

@Deprecated
public class UserInfo implements Serializable {

	private int id;
	private String login;
	private String full_name;
	private String email;
	private String avatar_url;
	private String language;
	private Boolean is_admin;
	private Date last_login;
	private Date created;
	private String login_name;
	private String password;
	private Boolean send_notify;
	private int source_id;
	private Boolean restricted;
	private Boolean active;
	private Boolean prohibit_login;
	private String location;
	private String website;
	private String description;
	private String visibility;
	private int followers_count;
	private int following_count;
	private int starred_repos_count;
	private String username;

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public UserInfo(String email, String full_name, String login_name, String password, String username, int source_id, Boolean send_notify) {
		this.email = email;
		this.full_name = full_name;
		this.login_name = login_name;
		this.password = password;
		this.username = username;
		this.source_id = source_id;
		this.send_notify = send_notify;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public int getId() {
		return id;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getLogin() {
		return login;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getFullname() {
		return full_name;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getEmail() {
		return email;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getAvatar() {
		return avatar_url;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getLang() {
		return language;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public String getUsername() {
		return username;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public Boolean getIs_admin() {
		return is_admin;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public Date getCreated() {
		return created;
	}

    /**
     * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
     */
	@Deprecated
	public Date getLast_login() {
		return last_login;
	}

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	public Boolean getRestricted() {
		return restricted;
	}

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	public Boolean getActive() {
		return active;
	}

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	public Boolean getProhibit_Login() {
		return prohibit_login;
	}

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	public String getLocation() {
		return location;
	}

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	public String getWebsite() {
		return website;
	}

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	public String getDescription() {
		return description;
	}

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	public String getVisibility() {
		return visibility;
	}

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	public int getFollowers_Count() {
		return followers_count;
	}

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	public int getFollowing_Count() {
		return following_count;
	}

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Deprecated
	public int getStarred_Repos_Count() {
		return starred_repos_count;
	}
}
