package org.gitnex.tea4j;

import org.gitnex.tea4j.annotations.Description;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Streaming;

/**
 * Author M M Arif
 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
 */

@SuppressWarnings("unused")
@Deprecated
public interface WebInterface {

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Streaming
	@Deprecated
	@GET("{owner}/{repo}/pulls/{index}.diff")
	@Description("get pull diff file contents")
	Call<ResponseBody> getPullDiffContent(@Header("Authorization") String auth, @Path("owner") String owner, @Path("repo") String repo,
		@Path("index") String pullIndex);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Streaming
	@Deprecated
	@GET("{owner}/{repo}/raw/branch/{branch}/{filepath}")
	@Description("get raw file contents")
	Call<ResponseBody> getFileContents(@Header("Authorization") String auth, @Path("owner") String owner, @Path("repo") String repo,
		@Path("branch") String branch, @Path(value = "filepath", encoded = true) String filepath);

	/**
	 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
	 */
	@Streaming
	@Deprecated
	@GET("{owner}/{repo}/commit/{sha}.diff")
	Call<ResponseBody> getCommitDiff(@Header("Authorization") String token, @Path("owner") String owner, @Path("repo") String repo,
		@Path("sha") String sha);

}
