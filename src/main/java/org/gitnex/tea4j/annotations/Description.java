package org.gitnex.tea4j.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author opyale
 * @deprecated tea4j is deprecated. Use <a href="https://codeberg.org/gitnex/tea4j-autodeploy">tea4j-autodeploy</a>.
 */

@Deprecated
@Retention(RetentionPolicy.CLASS)
@Target(ElementType.METHOD)
public @interface Description {

	String value();

}
