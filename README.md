# :warning: DEPRECATED

**tea4j was replaced by its successor [tea4j-autodeploy](https://codeberg.org/gitnex/tea4j-autodeploy) which uses auto-generated classes. tea4j won't receive new updates.**

[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0) [![Releases](https://img.shields.io/badge/dynamic/json.svg?label=release&url=https://codeberg.org/api/v1/repos/gitnex/tea4j/releases&query=$[0].tag_name)](https://codeberg.org/gitnex/tea4j/releases) [![Join the Discord chat at https://discord.gg/FbSS4rf](https://img.shields.io/discord/632219664587685908.svg)](https://discord.gg/FbSS4rf) [![status-badge](https://ci.codeberg.org/api/badges/gitnex/tea4j/status.svg)](https://ci.codeberg.org/gitnex/tea4j)

# tea4j

A Java SDK for the Gitea API

![tea4j](https://codeberg.org/gitnex/tea4j/raw/branch/main/assets/logo-tea4j-sm-80.png)

**Note: change VERSION to version from releases page**

#### MAVEN
Add dependency to your `pom.xml` file:

```xml
<repositories>
    <repository>
        <id>jitpack.io</id>
        <url>https://jitpack.io</url>
    </repository>
</repositories>
```

```xml
<dependencies>
    <dependency>
        <groupId>org.codeberg.gitnex</groupId>
        <artifactId>tea4j</artifactId>
        <version>VERSION</version>
    </dependency>
</dependencies>
```

#### GRADLE
Add to the dependencies section:

```groovy
implementation 'org.codeberg.gitnex:tea4j:VERSION'
```

#### USAGE
Check the [GitNex Android app repository](https://codeberg.org/gitnex/GitNex) for more details or join the [discord chat](https://discord.gg/FbSS4rf).